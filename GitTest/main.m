//
//  main.m
//  GitTest
//
//  Created by Adrian Watzlawczyk on 04/09/14.
//  Copyright (c) 2014 ___ADRIANWATZLAWCZYK___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AWAppDelegate class]));
    }
}
